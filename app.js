const request = require('request');

/* flags to indicate if the notifications are sent or not */
let notify_normal = false;
let notify_outside = false;

const getCurrentTempartureByNameAPI = function (city_name) {
    return new Promise((resolve, reject) => {
        request.get({
            url: `http://api.openweathermap.org/data/2.5/find?q=${city_name}&units=metric&appid=c9d9c5a3e31e1a07f0224ce247667891`,
        }, function (error, response, body) {
            if (error) {
                console.error('getCurrentTempartureByNameAPI ERROR', error);
                return reject(err);
            }
            return resolve(body);
        });
    })
}

/**
    function to get the current temperature by passing the city_name 
    @inputParams city_name
**/
const getCurrentTempartureByName = function (city_name) {
    return new Promise((resolve, reject) => {
        return getCurrentTempartureByNameAPI(city_name)
            .then((response) => {
                console.info('getCurrentTempartureByNameAPI response', response);
                let temp = JSON.parse(response).list[0].main.temp;
                return resolve(temp);
            })
            .catch((error) => {
                console.error(' getCurrentTempartureByName', error);
                return reject(error);
            })
    })
}

/**
    function to notify the user for the temperature change
    @inputParams curr_temp current temperature of the city
    @inputParams inRange flag to define if the current temperature in is the range provided by the user or not

**/
const notifyUserAPI = (curr_temp, inRange) => {
    return new Promise((resolve, reject) => {
        request.post({
            url: `https://reqres.in/api/notifyUser/1`,
            headers: 'content-type : application/json',
            body: JSON.stringify({ current_temperature: curr_temp })
        }, function (error, response, body) {
            if (error) {
                console.error('getCurrentTempartureByNameAPI ERROR', error);
                return reject(err);
            }
            if (!inRange) {
                notify_outside = true;
                notify_normal = false;
            } else {
                notify_outside = false;
                notify_normal = true;
            }
            console.log('notifyUserAPI response', body);
            return resolve(body);
        });
    })
}


const getTemperatureNotification = (city_name, min_temp, max_temp) => {
    return new Promise((resolve, reject) => {
        // validating all the required inputs
        if (!city_name) {
            return reject(new Error('city_name is required'));
        }
        if (!min_temp) {
            return reject(new Error('min_temp is required'));
        }
        if (!max_temp) {
            return reject(new Error('max_temp is required'));
        }

        return getCurrentTempartureByName(city_name)
            .then((curr_temp) => {
                console.info(`current_temp of ${city_name} is ${curr_temp} degree celsis at ${new Date()}`);
                let inRange = false;
                if (curr_temp >= min_temp && curr_temp <= max_temp) {
                    if (!notify_normal) {
                        inRange = true;
                        return notifyUserAPI(curr_temp, inRange);
                    }
                    console.log('user is already notified for normal temperature,skipping notification')
                    return Promise.resolve(true);
                }
                else {
                    if (!notify_outside) {
                        return notifyUserAPI(curr_temp, inRange);
                    }
                    console.log('user is already notified for outside range temperature,skipping notification')
                    return Promise.resolve(true);
                }
            })
            .then((response) => {
                return resolve(response);
            })
            .catch((err) => {
                console.error(err);
                return reject(err);
            })
    })
};



const TemperatureNotification = () => {
    console.info('application is now running, it will notify the user for the climate change after every 10 mins')
    
    getTemperatureNotification('Noida', '10', '45');
    
    //running the code after every 10 mins
    setInterval(function () {
        console.info(`running the code at ${new Date()}`)
        getTemperatureNotification('Noida', '10', '45');
    }, 600000);
}

TemperatureNotification();